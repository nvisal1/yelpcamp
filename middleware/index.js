var comment    = require('../models/comment'),
    post       = require('../models/post');

// All Middleware 
var middlewareObject = {};

middlewareObject.isLoggedIn = function (req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    req.flash('error', 'You must be logged in to do that.');
    res.redirect('/login');
}

middlewareObject.isCommentAuthor = function (req, res, next) {
    comment.findById(req.params.cid, function(error, comment) {
        if(error) {
            req.flash('error', 'We could not find that comment.');
            res.redirect('back');
        } else {
            if(req.user.username === comment.author.username) {
                return next();
            }
            req.flash('error', 'You must be the author of the comment to do that.');
            res.redirect('back');
        }
    });
}  

middlewareObject.isPostAuthor = function (req, res, next) {
    post.findById(req.params.id, function(error, post) {
        if(error) {
            req.flash('error', 'We could not find that post.');
            res.redirect('back');
        } else {
            if(req.user.username === post.author.username) {
                return next();
            }
            req.flash('error', 'You must be the author of the post to do that.');
            res.redirect('back');
        }
    });
}

module.exports = middlewareObject;
   


 