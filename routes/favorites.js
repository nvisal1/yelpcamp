var express     = require('express'),
    router      = express.Router(),
    passport    = require('passport'),
    user        = require('../models/user');
    post        = require('../models/post');
    middleware = require('../middleware/index');

// *** Auth routes ***
// Get all favorite recipes for a specific user
router.get('/', function(req, res) {
    res.render('authentication/register');
});

// Append favorite recipe to a user
router.post('/:id', middleware.isLoggedIn, function(req, res) {
    post.findById(req.params.id, function(error, post) {
        if(error) {
            req.flash('error', error.message);
            res.redirect('back');
        } else {
            req.user.favorites.push(post);
            req.user.save();
            req.flash('success', 'Successfully added ' + post.name + ' as a favorite!');
            res.redirect('back');
        }
    });
});


// Export router
module.exports = router;