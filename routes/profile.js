var express    = require('express'),
    router     = express.Router(),
    post       = require('../models/post'),
    middleware = require('../middleware/index');

// Display user profile page
router.get('/', middleware.isLoggedIn, function(req, res) {
    res.render('profile/profile');
});

// Export router
module.exports = router;