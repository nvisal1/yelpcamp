var express    = require('express'),
    router     = express.Router({ mergeParams: true }),
    comment    = require('../models/comment'),
    post       = require('../models/post'),
    middleware = require('../middleware/index');

// display new comment form
router.get('/new', middleware.isLoggedIn, function(req, res) {
    res.render('comments/new', { postId: req.params.id });
});

// Create new comment
router.post('/', middleware.isLoggedIn, function(req, res) {
    post.findById(req.params.id, function(error, post){
        if(error){
            console.log('Sorry, we could not find your post!');
        } else {
            comment.create(req.body, function(error, comment){
                if(error) {
                    console.log('Sorry, we could not create your comment!');
                } else {
                    comment.author.id = req.user._id;
                    comment.author.username = req.user.username;
                    comment.save();
                    post.comments.push(comment);
                    post.save();   
                }
                res.redirect('/recipes/' + req.params.id);
            });
        }
    });
});

// display edit comment form
router.get('/:cid/edit', middleware.isLoggedIn, middleware.isCommentAuthor, function(req, res) {
    comment.findById(req.params.cid, function(error, comment) { 
        if(error) {
            res.redirect('back');
        } else {
            res.render('comments/edit', { postId: req.params.id, comment: comment});
        }
    });
});

// Update existing comment
router.put('/:cid', middleware.isLoggedIn, middleware.isCommentAuthor, function(req, res) {
   comment.findByIdAndUpdate(req.params.cid, req.body, function(error, comment) {
        if(error) {
            res.redirect('back');
        } else{
            res.redirect('/recipes/' + req.params.id);
        }
   });
});

// delete existing comment
router.delete('/:cid', middleware.isLoggedIn, middleware.isCommentAuthor, function(req, res) {
    comment.findByIdAndRemove(req.params.cid, function(error, comment) {
         res.redirect('back');
    });
 });

// Export router
module.exports = router;