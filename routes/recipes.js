var express    = require('express'),
    router     = express.Router(),
    post       = require('../models/post'),
    middleware = require('../middleware/index');

// Display all posts
router.get('/', function(req, res) {
    post.find({}, function(error, posts) {
        if(error) {
            console.log('We could not retrieve all posts :(');
        } else {
            res.render('recipes/recipes', {recipes: posts});
        }
    });
});

// Create a new post
router.post('/', middleware.isLoggedIn, function(req, res) {
    post.create(req.body, function(error, post) {
        if(error){
            console.log('We could not create your post :(');
        } else {
            post.author.id = req.user._id;
            post.author.username = req.user.username;
            post.save();
            res.redirect('/recipes');
        }
    });
});

// Display new post form
router.get('/new', middleware.isLoggedIn, function(req, res) {
    res.render('recipes/new');
});

// Display show page for a single post
router.get('/:id', function(req, res) {
    post.findById(req.params.id).populate('comments').exec(function(error, post){
        if(error) {
            console.log("Something went wrong :(");
            res.redirect('/');
        } else {
            res.render('recipes/show', {post: post});
        }
    });
});

// Display new edit form
router.get('/:id/edit', middleware.isLoggedIn, middleware.isPostAuthor, function(req, res) {
    post.findById(req.params.id, function(error, post) {
        if(error) {
            console.log(error);
            res.redirect('/');
        } else {
            res.render('recipes/edit', { post: post});
        }
    }) ;
});

// edit existing post
router.put('/:id', middleware.isPostAuthor, function(req, res) {
    post.findByIdAndUpdate(req.params.id, req.body, function(error, post){
        if(error) {
            console.log("Something went wrong :(");
            res.redirect('/');
        } else {
            res.redirect('/recipes/' + req.params.id);
        }
    });
});

// delete existing post
router.delete('/:id', middleware.isPostAuthor, function(req, res) {
    post.findByIdAndRemove(req.params.id, function(error, post){
        if(error) {
            console.log("Something went wrong :(");
            res.redirect('/');
        } else {
            res.redirect('/recipes');
        }
    });
});

// Export router
module.exports = router;