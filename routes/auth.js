var express     = require('express'),
    router      = express.Router(),
    passport    = require('passport'),
    User        = require('../models/user');

// *** Root route ***
router.get('/', function(req, res) {
    res.render('landing');
});

// *** Auth routes ***
router.get('/register', function(req, res) {
    res.render('authentication/register');
});

router.post('/register', function(req, res) {
    User.register({username: req.body.username, email: req.body.email}, req.body.password, function(error, user){
        if(error) {
            req.flash('error', error.message);
            res.redirect('/register');
        } else {
            passport.authenticate('local')(req, res, function() {
                req.flash('success', 'Account successfully created. Welcome to Yelp Clone ' + user.username + '!');
                res.redirect('/');
            });
        }
    });
});

router.get('/login', function(req, res) {
    res.render('authentication/login');
});

router.post('/login', passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login'
    }),function(req, res) {
});

router.get('/logout', function(req, res) {
    req.flash('success', 'Successfully logged out');
    req.logout();
    res.redirect('/');
});

// Export router
module.exports = router;