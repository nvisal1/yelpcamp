var mongoose    = require('mongoose'),
    post        = require('./models/post'),
    comment     = require('./models/comment')

fillPost = {
    name: 'Example Place',
    description: '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."',
    image: 'https://images.unsplash.com/photo-1424296308064-1eead03d1ad9?ixlib=rb-0.3.5&s=98963ffaa96835d8173a7d4868b3d82f&auto=format&fit=crop&w=1500&q=80'
};

fillComment = { 
    text: 'Wow! This place looks awesome!',
    author: 'Nick Visalli'
};


module.exports = function seedDB() {
    post.remove({}, function(error) {
        if(error) {
            console.log(error);
        } else {
            console.log('The database was cleared');
            for(var i = 0; i < 100; i++) { 
                post.create(fillPost, function(error, newPost){
                    if(error) {
                        console.log(error);
                    } else {
                        comment.create(fillComment, function(error, newComment){
                            if(error) {
                                console.log('Comment could not be created.');
                            } else {
                                newPost.comments.push(newComment);
                                newPost.save();
                            }
                        });
                    }
                });
            }
            console.log('The database has been seeded =^)');
        }
    });
}