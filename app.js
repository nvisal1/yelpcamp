var express        = require('express'), 
    md5            = require('md5'),
    app            = express(),
    User           = require('./models/user'),
    post           = require('./models/post'),
    flash          = require('connect-flash'),
    seedDb         = require('./seed'),
    comment        = require('./models/comment'),
    mongoose       = require('mongoose'),
    passport       = require('passport'),
    bodyParser     = require('body-parser'),
    LocalStrategy  = require('passport-local'),
    expressSession = require('express-session'),
    methodOverride = require('method-override');

// Routes
var recipeRoutes = require('./routes/recipes'),
    commentroutes  = require('./routes/comments'),
    profileroutes  = require('./routes/profile'),
    authRoutes     = require('./routes/auth');
    favoriteRoutes   = require('./routes/favorites');

// PORT 
const PORT = process.env.PORT || 3000;
const DB = process.env.DATABASEURL || 'mongodb://localhost/yelp_db';

// Seed script for development
// seedDb();

// Misc configuration
app.use(bodyParser.urlencoded({ extended: true}))
app.use(express.static('public'));
app.use(expressSession({
    secret: "This is a Yelp Clone!",
    resave: false,
    saveUninitialized: false
    })
);
app.use(flash());
app.use(methodOverride('_method'));
app.use(passport.initialize());
app.use(passport.session()); 

// Global variables
app.use(function(req, res, next) {
    res.locals.user = req.user;
    res.locals.error = req.flash('error');
    res.locals.success = req.flash('success');
    if(req.user) {
        res.locals.hash = md5(req.user.email);
    }
    next();
});

// Passport configuration
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

mongoose.connect(DB);

app.set('view engine', 'ejs');

// Route Config
app.use('/recipes', recipeRoutes);
app.use('/recipes/:id/comments', commentroutes);
app.use('/profile', profileroutes);
app.use('/favorites', favoriteRoutes);
app.use(authRoutes);

app.listen(PORT, () => console.log(`Listening on ${ PORT }`));
