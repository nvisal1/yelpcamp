var mongoose = require('mongoose');

var commentSchema = new mongoose.Schema({
    text: String,
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        username: String // We could just store id, but we will be printing out username often.
    }
});

module.exports = mongoose.model("Comment", commentSchema);